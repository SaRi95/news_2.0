import { Component, OnInit } from '@angular/core';
import { NewsService } from '../news.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-science',
  templateUrl: './science.page.html',
  styleUrls: ['./science.page.scss'],
})
export class SciencePage implements OnInit {
  items: any;
  public search: string;

  constructor(private newsService: NewsService,
    private router: Router) { }

  ngOnInit() {
    this.newsService.getData('top-headlines?country=us&category=science').subscribe(data => {
      this.items = data;
    })
  }

  openNews(article) {
    this.newsService.currentNews = article;
    this.router.navigate(['/details']);
  }
}
