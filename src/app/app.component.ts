import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  navigate: any;
  public category: string;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.sideMenu();
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  sideMenu()
  {
    this.navigate =
    [
      {
        title: "All",
        url: "/home",
      },

      {
        title: "Business",
        url: "/business",
      },
      {
        title : "Entertainment",
        url   : "/entertainment",
      },
      {
        title : "Health",
        url   : "/health",
      },

      {
        title : "Science",
        url   : "/science",
      },

      {
        title : "Sports",
        url   : "/sports",
      },

      {
        title : "Technology",
        url   : "/technology",
      },
    ]
  }
  
  changeTheme() {
    document.body.classList.toggle('dark');
  }
}
