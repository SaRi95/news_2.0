import { Component, OnInit } from '@angular/core';
import { NewsService } from '../news.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-technology',
  templateUrl: './technology.page.html',
  styleUrls: ['./technology.page.scss'],
})
export class TechnologyPage implements OnInit {
  items: any;
  public search: string;

  constructor(private newsService: NewsService,
    private router: Router) { }

  ngOnInit() {
    this.newsService.getData('top-headlines?country=us&category=technology').subscribe(data => {
      this.items = data;
    })
  }

  openNews(article) {
    this.newsService.currentNews = article;
    this.router.navigate(['/details']);
  }
}
